$(document).ready(function() {
    var from;
    var to;

    $('td').click(function() {
        if (!from) {
            from = $(this).attr('id');
        } else if (!to) {
            to = $(this).attr('id');

            window.location = "./move?from=" + from.toLowerCase() + "&to=" + to.toLowerCase()
        }
    });
});
