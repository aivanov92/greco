module Greco
  module Chess
    class Game
      attr_reader :board, :scoresheet, :result

      def initialize(board = nil)
        @board = board || Board.create_default_board
        @scoresheet = @board.scoresheet
      end

      def make_move(move)
        return false if result

        player = @scoresheet.player_on_move

        validator = Validator.new @board, move

        return false unless validator.check?

        @board.apply_move move

        opponent = @scoresheet.player_on_move

        if not has_any_moves? opponent
          if @board.get_king_of(opponent).field.attacked_by? player
            @result = player == :white ? :WHITE_WIN : :BLACK_WIN
          else
            @result = :DRAW
          end
        elsif fifty_moves_no_gain?
          @result = :DRAW
        end

        true
      end

      def has_any_moves?(color)
        pieces = if color == :white
          @board.white_pieces
        else
          @board.black_pieces
        end

        pieces.any? { |piece| not piece.moves.empty? }
      end

      def fifty_moves_no_gain?
        return false unless @scoresheet.moves.size > 50

        total_basic_moves = 0

        check_move = Proc.new do |move|
          if move
            if move.piece.is_a? Piece::Pawn or move.captured
              return false
            else
              total_basic_moves += 1
            end
          end
        end

        @scoresheet.moves.reverse_each do |moves_pair|
          black_move = moves_pair[:black]
          white_move = moves_pair[:white]

          check_move.call black_move
          check_move.call white_move
        end
      end
    end
  end
end
