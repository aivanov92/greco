module Greco
  module Chess
    class Scoresheet
      attr_accessor :moves

      def initialize
        @moves = []
      end

      def last_move(color = nil)
        if color.nil?
          return unless @moves.last

          @moves.last[:black] || @moves.last[:white]
        elsif color == :white
          last_white_move
        else
          last_black_move
        end
      end

      def last_black_move
        if @moves[-1]
          @moves[-1][:black] || (@moves[-2] ? @moves[-2][:black] : nil)
        end
      end

      def last_white_move
        if @moves[-1]
          @moves[-1][:white] || (@moves[-2] ? @moves[-2][:white] : nil)
        end
      end

      def add_move(move)
        meta_move = move
        unless meta_move.respond_to?('previous_move=')
          meta_move = MetaMove.new move
        end

        if player_on_move == :white
          meta_move.previous_move = last_black_move
          @moves << { white: meta_move }
        else
          meta_move.previous_move = last_white_move
          @moves.last[:black] = meta_move
        end
      end

      def pop_move
        return unless @moves.last

        result = if @moves.last[:black]
          @moves.last.delete :black
        else
          @moves.last.delete :white
        end

        @moves.pop if @moves.last.empty?

        result
      end

      def player_on_move
        current_move = @moves.last

        if !current_move or current_move[:black]
          :white
        else
          :black
        end
      end
    end
  end
end
