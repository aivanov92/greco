module Greco
  module Chess
    class MetaMove
      attr_accessor :from, :to, :promote, :piece, :captured, :previous_move

      def initialize(move, previous_move = nil)
        @from = Coordinates.new move.from
        @to = Coordinates.new move.to
        @promote = move.promote
        @previous_move = previous_move
      end

      def looks_like_en_passant?
        return false unless @previous_move and @piece.is_a?(Piece::Pawn)

        move_from = @from.to_120
        move_to = @to.to_120
        step = move_to - move_from

        return false if step.abs != 9 and step.abs != 11

        last_move_from = @previous_move.from.to_120
        last_move_to = @previous_move.to.to_120

        if @piece.color == :white
          unless move_from.between?(51, 58) and
                 last_move_from == move_to - 10 and
                 last_move_to == move_to + 10
            return false
          end
        else
          unless move_from.between?(61, 68) and
                 last_move_from == move_to + 10 and
                 last_move_to == move_to - 10
            return false
          end
        end

        return true
      end

      def is_en_passant?
        @captured.is_a?(Piece::Pawn) and looks_like_en_passant?
      end

      def is_castling?
        return false unless @piece and @piece.is_a? Piece::King

        if @piece.color == :white
          @from == :e1 and (@to == :g1 or @to == :c1)
        else
          @from == :e8 and (@to == :g8 or @to == :c8)
        end
      end
    end
  end
end
