module Greco
  module Chess
    module Piece
      class BasePiece
        attr_reader :color, :field, :board
        attr_accessor :times_moved

        def initialize(color, field = nil)
          unless color == :black or color == :white
            raise ArgumentError "unknown piece color #{color}"
          end

          @color = color
          @times_moved = 0
        end

        def set_board(board)
          @board = board
        end

        def set_field(field)
          @field = field
        end

        def field=(field)
          if @field
            @field.set_piece nil
          end

          if field.nil?
            @field = nil
          else
            @field = field
            @board = field.board
            @field.set_piece self
          end
        end

        def moved?
          @times_moved != 0
        end

        def black?
          @color == :black
        end

        def white?
          @color == :white
        end

        def attacks
          []
        end

        def attacks?(field)
          attacks.include? field
        end

        def to_s
          "#{color} #{self.class.name}"
        end
      end
    end
  end
end
