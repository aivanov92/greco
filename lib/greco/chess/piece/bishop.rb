module Greco
  module Chess
    module Piece
      class Bishop < BasePiece
        include SlidingAttack
        include BasicMovement

        def initialize(*params)
          super *params

          @sliding_steps = [11, 9, -11, -9]
        end

        alias :attacks :attacks_when_sliding

        alias :moves :basic_moves
      end
    end
  end
end
