module Greco
  module Chess
    module Piece
      class Queen < BasePiece
        include SlidingAttack
        include BasicMovement

        def initialize(*params)
          super *params

          @sliding_steps = [10, 1, -10, -1, 11, 9, -11, -9]
        end

        alias :attacks :attacks_when_sliding
        alias :moves :basic_moves
      end
    end
  end
end
