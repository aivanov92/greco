require 'spec_helper'

module Greco
  module Chess
    describe Game do
      include TestUtils

      describe 'Stalemate with queen and knight' do
        it do
          board = create_board %w(
            -- -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- bN
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- bQ -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- wK --
          )
          board.scoresheet.moves = [ { white: Move.new(:h2, :g1) } ]

          game = Game.new board

          game.make_move Move.new(:h6, :g4)

          result = create_board %w(
            -- -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- bN --
            -- -- -- -- -- bQ -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- wK --
          )

          game.board.to_64.should == result.to_64
          game.result.should be :DRAW
        end
      end
    end
  end
end
