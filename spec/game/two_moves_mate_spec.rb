require 'spec_helper'

module Greco
  module Chess
    describe Game do
      include TestUtils

      describe 'most stupid mate ever..' do
        it do
          game = Game.new

          [
            [:f2, :f3], [:e7, :e5],
            [:g2, :g4], [:d8, :h4]
          ].each do |from, to|
            game.make_move(Move.new from, to).should be true
          end

          result = create_board %w(
            bR bN bB -- bK bB bN bR
            bP bP bP bP -- bP bP bP
            -- -- -- -- -- -- -- --
            -- -- -- -- bP -- -- --
            -- -- -- -- -- -- wP bQ
            -- -- -- -- -- wP -- --
            wP wP wP wP wP -- -- wP
            wR wN wB wQ wK wB wN wR
          )

          game.board.to_64.should == result.to_64
          game.result.should be :BLACK_WIN
        end
      end
    end
  end
end
