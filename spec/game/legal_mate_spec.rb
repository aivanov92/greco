require 'spec_helper'

module Greco
  module Chess
    describe Game do
      include TestUtils

      describe 'Legal\'s mate' do
        it do
          game = Game.new

          [
            [:e2, :e4], [:e7, :e5],
            [:f1, :c4], [:d7, :d6],
            [:g1, :f3], [:c8, :g4],
            [:b1, :c3], [:g7, :g6],
            [:f3, :e5], [:g4, :d1],
            [:c4, :f7], [:e8, :e7],
            [:c3, :d5],
          ].each do |from, to|
            game.make_move(Move.new from, to).should be true
          end

          result = create_board %w(
            bR bN -- bQ -- bB bN bR
            bP bP bP -- bK wB -- bP
            -- -- -- bP -- -- bP --
            -- -- -- wN wN -- -- --
            -- -- -- -- wP -- -- --
            -- -- -- -- -- -- -- --
            wP wP wP wP -- wP wP wP
            wR -- wB bB wK -- -- wR
          )

          game.board.to_64.should == result.to_64
          game.result.should be :WHITE_WIN
        end
      end
    end
  end
end
