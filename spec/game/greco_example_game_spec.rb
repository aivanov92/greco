require 'spec_helper'

module Greco
  module Chess
    describe Game do
      include TestUtils

      describe 'Greco example game' do
        it do
          game = Game.new

          [
            [:e2, :e4], [:e7, :e5],
            [:g1, :f3], [:b8, :c6],
            [:f1, :c4], [:f8, :c5],
            [:c2, :c3], [:g8, :f6],
            [:d2, :d4], [:e5, :d4],
            [:c3, :d4], [:c5, :b4],
            [:b1, :c3], [:f6, :e4],
            [:e1, :g1], [:e4, :c3],
            [:b2, :c3], [:b4, :c3],
            [:d1, :b3], [:c3, :d4],
            [:c4, :f7], [:e8, :f8],
            [:c1, :g5], [:d4, :f6],
            [:a1, :e1], [:c6, :e7],
            [:f7, :h5], [:e7, :g6],
            [:f3, :e5], [:g6, :e5],
            [:e1, :e5], [:g7, :g6],
            [:g5, :h6], [:f6, :g7],
            [:e5, :f5], [:g6, :f5],
            [:b3, :f7]
          ].each do |from, to|
            game.make_move(Move.new from, to).should be true
          end

          result = create_board %w(
            bR -- bB bQ -- bK -- bR
            bP bP bP bP -- wQ bB bP
            -- -- -- -- -- -- -- wB
            -- -- -- -- -- bP -- wB
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            wP -- -- -- -- wP wP wP
            -- -- -- -- -- wR wK --
          )

          game.board.to_64.should == result.to_64
          game.result.should be :WHITE_WIN
        end
      end
    end
  end
end
