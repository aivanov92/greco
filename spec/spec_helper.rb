require 'rspec'
require 'greco'

module TestUtils
  def is_ok_to_move?(board, move)
    validator = Greco::Chess::Validator.new board, move
    validator.check?.should be true
  end

  def is_not_ok_to_move?(board, move)
    validator = Greco::Chess::Validator.new board, move
    validator.check?.should be false
  end

  def create_board(board64)
    board = Greco::Chess::Board.new

    board64.each_with_index do |field, i|
      row = i / 8

      if field != '--'
        pos = i + 21 + 2*row

        piece = parse_piece field

        board.set pos => piece
      end
    end

    board
  end

  private

  def parse_piece(name)
    color = :white
    if name[0] == 'b'
      color = :black
    end

    case name[1]
      when 'K' then Greco::Chess::Piece::King.new color
      when 'P' then Greco::Chess::Piece::Pawn.new color
      when 'N' then Greco::Chess::Piece::Knight.new color
      when 'B' then Greco::Chess::Piece::Bishop.new color
      when 'R' then Greco::Chess::Piece::Rook.new color
      when 'Q' then Greco::Chess::Piece::Queen.new color
    end
  end
end
