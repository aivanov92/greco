require 'spec_helper'

module Greco
  module Chess
    describe MetaMove do
      describe '#is_castling?' do
        it 'is short castle with white' do
          move = Move.new :e1, :g1

          meta_move = MetaMove.new move
          meta_move.piece = Piece::King.new :white

          meta_move.is_castling?.should be true
        end

        it 'is long castle with white' do
          move = Move.new :e1, :c1

          meta_move = MetaMove.new move
          meta_move.piece = Piece::King.new :white

          meta_move.is_castling?.should be true
        end

        it 'is short castle with black' do
          move = Move.new :e8, :g8

          meta_move = MetaMove.new move
          meta_move.piece = Piece::King.new :black

          meta_move.is_castling?.should be true
        end

        it 'is short castle with black' do
          move = Move.new :e8, :c8

          meta_move = MetaMove.new move
          meta_move.piece = Piece::King.new :black

          meta_move.is_castling?.should be true
        end

        it 'is not a castling' do
          move = Move.new :d8, :c8

          meta_move = MetaMove.new move
          meta_move.piece = Piece::King.new :black

          meta_move.is_castling?.should be false
        end
      end

      describe '#is_en_passant?' do
        it 'works ok for white' do
          black_move = Move.new :d7, :d5
          meta_move_black = MetaMove.new black_move
          meta_move_black.piece = Piece::Pawn.new :black

          move = Move.new :e5, :d6
          meta_move_white = MetaMove.new move
          meta_move_white.piece = Piece::Pawn.new :white
          meta_move_white.captured = meta_move_black.piece
          meta_move_white.previous_move = meta_move_black

          meta_move_white.is_en_passant?.should be true
        end
      end
    end
  end
end
