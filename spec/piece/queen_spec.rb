require 'spec_helper'

module Greco
  module Chess
    module Piece
      describe Queen do
        include TestUtils

        describe '#attacks' do
          let (:board) { Board.new }

          it 'diagonals and varitcal and horizontal lines' do
            queen = Queen.new :black
            board.set c4: queen

            queen.attacks.should include :a4, :b4, :d4, :e4, :f4, :g4, :h4,
                                         :c8, :c7, :c6, :c5, :c3, :c2, :c1,
                                         :b3, :a2, :b5, :a6, :d3, :e2, :f1,
                                         :d5, :e6, :f7, :g8

            queen.attacks.size.should be 25
          end

          it 'till a figure is hit' do
            queen = Queen.new :black
            board.set c4: queen, c6: Pawn.new(:white), e6: Pawn.new(:white)

            queen.attacks.should include :a4, :b4, :d4, :e4, :f4, :g4, :h4,
                                         :c6, :c5, :c3, :c2, :c1,
                                         :b3, :a2, :b5, :a6, :d3, :e2, :f1,
                                         :d5, :e6

            queen.attacks.size.should be 21
          end
        end

        describe '#moves' do
          it do
            board = create_board %w(
              -- -- -- -- bK -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- bR -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- wQ --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- wK -- -- --
            )

            possible_moves = board[:g4].piece.moves

            possible_moves.should include :e2, :e6, :e4
            possible_moves.size.should be 3
          end
        end
      end
    end
  end
end
