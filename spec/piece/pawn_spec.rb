require 'spec_helper'

module Greco
  module Chess
    module Piece
      describe Pawn do
        include TestUtils

        describe '#attacks' do
          let(:board) { Board.new }

          context 'when white' do
            let(:pawn) { Pawn.new :white }

            it 'attacks multiple fields from the center' do
              board.set d4: pawn
              pawn.attacks.should include :c5, :e5
              pawn.attacks.size.should be 2
            end

            describe 'does not hit outside the board' do
              it do
                board.set h2: pawn
                pawn.attacks.should eq [:g3]
              end

              it do
                board.set a2: pawn
                pawn.attacks.should eq [:b3]
              end
            end
          end

          context 'when black' do
            let(:pawn) { Pawn.new :black }

            it 'attacks multiple fields from the center' do
              board.set d4: pawn
              pawn.attacks.should include :e3, :c3
              pawn.attacks.size.should be 2
            end

            describe 'does not hit outside the board' do
              it do
                board.set h2: pawn
                pawn.attacks.should eq [:g1]
              end

              it do
                board.set a7: pawn
                pawn.attacks.should eq [:b6]
              end
            end
          end
        end

        describe '#moves' do
          it do
            board = create_board %w(
              -- -- -- -- bK -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- bP -- -- -- --
              bR bP wP -- -- wK -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- -- --
            )

            board.scoresheet.moves = [ { black: Move.new(:b7, :b5) } ]

            possible_moves = board[:c5].piece.moves

            possible_moves.should include :c6, :d6
            possible_moves.size.should be 2
          end

          it do
            board = create_board %w(
              -- -- -- -- bK -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- wK -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- wP -- -- -- --
              -- -- -- -- -- -- -- --
            )

            board.scoresheet.moves = [ { black: Move.new(:d8, :e8) } ]

            possible_moves = board[:d2].piece.moves

            possible_moves.should include :d3, :d4
            possible_moves.size.should be 2
          end
        end
      end
    end
  end
end
