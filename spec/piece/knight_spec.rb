require 'spec_helper'

module Greco
  module Chess
    module Piece
      describe 'Knight' do
        include TestUtils

        describe '#attacks' do
          let (:board) { Board.new }

          it do
            knight = Knight.new :black
            board.set c4: knight

            knight.attacks.should include :a5, :b6, :d6, :e5, :e3, :d2, :b2, :a3
            knight.attacks.size.should eq 8
          end

          it 'not outside borders' do
            knight = Knight.new :black
            board.set h8: knight

            knight.attacks.should include :f7, :g6
            knight.attacks.size.should eq 2
          end
        end

        describe '#moves' do
          it do
            board = create_board %w(
              -- -- -- -- bK -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- bQ -- -- --
              -- -- wN -- -- -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- wK -- -- --
            )

            possible_moves = board[:c4].piece.moves

            possible_moves.should include :e5, :e3
            possible_moves.size.should be 2
          end
        end
      end
    end
  end
end
