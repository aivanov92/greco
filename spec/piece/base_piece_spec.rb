require 'spec_helper'

module Greco
  module Chess
    module Piece
      describe BasePiece do
        it 'could be created only with white or black color' do
          BasePiece.new :white
          BasePiece.new :black

          expect { BasePiece.new :green }.to raise_error
        end

        let(:board) { Board.new }
        let(:piece) { BasePiece.new :white }

        context 'when it\'s field is changed' do
          it 'will force the field to change it\'s piece' do
            piece.field = board[:e4]

            board[:e4].piece.should be piece
          end

          it 'will update is\'s board' do
            piece.field = board[:e4]

            piece.board.should be board
          end
        end

        it '#set_field' do
          piece.set_field board[:e4]

          board[:e4].piece.should be nil
        end

        it 'does not attack anyone' do
          piece.field = board[:d4]

          piece.attacks.should be_empty
        end
      end
    end
  end
end
