require 'spec_helper'

module Greco
  module Chess
    module Piece
      describe Bishop do
        include TestUtils

        describe '#attacks' do
          let (:board) { Board.new }

          it 'should attack diagonals' do
            bishop = Bishop.new :white

            board.set g2: bishop

            bishop.attacks.should include :h3, :h1, :f1, :f3, :e4, :d5, :c6, :b7, :a8
            bishop.attacks.size.should be 9
          end

          it 'till another figure is hit' do
            bishop = Bishop.new :white

            board.set g2: bishop, f3: Pawn.new(:black)

            bishop.attacks.should include :h3, :h1, :f1, :f3
            bishop.attacks.size.should be 4
          end
        end

        describe '#moves' do
          it do
            board = create_board %w(
              -- -- -- -- bK -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- bR -- -- --
              -- -- -- -- -- -- -- --
              -- -- wB -- -- -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- -- -- -- --
              -- -- -- -- wK -- -- --
            )

            possible_moves = board[:c4].piece.moves

            possible_moves.should include :e2, :e6
            possible_moves.size.should be 2
          end
        end
      end
    end
  end
end
