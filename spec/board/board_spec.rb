require 'spec_helper'

module Greco
  module Chess
    describe Board do
      include TestUtils

      describe '#new' do
        it { should be_all &:empty? }

        it 'could be created with pieces' do
          board = Board.new e4: Piece::Pawn.new(:white),
                            d4: Piece::Rook.new(:white)

          board[:e4].piece.should be_a Piece::Pawn
          board[:d4].piece.should be_a Piece::Rook
        end
      end

      it '#to_120' do
        board = Board.new.to_120

        board.size.should eq 120
        board[0].should be nil
        board[66].should be_a Board::Field
        board[66].should eq 66
      end

      it '#to_64' do
        board = Board.new.to_64

        board.size.should eq 64
        board.should be_all { |field| field.is_a? Board::Field }
      end

      it 'could be iterated only over the fields' do
        should be_all { |field| field.is_a? Board::Field }
      end

      its '#set' do
        board = Board.new

        pawn = Piece::Pawn.new :white
        king = Piece::King.new :white

        board.set e2: pawn, e1: king

        e1_field, e2_field = board.get :e1, :e2

        e1_field.piece.should be king
        e2_field.piece.should be pawn
      end

      describe 'accessors' do
        let (:board) { Board.new }

        its '#[]' do
          board[:e4].should be_a Board::Field
          board[:e4].should eq :e4
        end

        its '#get' do
          fields = board.get :e2, :e3, :e4

          fields.should eq [:e2, :e3, :e4]
        end

        its '#upper_left_diagonal_from' do
          fields = board.upper_left_diagonal_from :f5

          fields.should eq [:g6, :h7]
        end

        its '#lower_left_diagonal_from' do
          fields = board.lower_left_diagonal_from :c4

          fields.should eq [:d3, :e2, :f1]
        end

        its '#upper_right_diagonal_from' do
          fields = board.upper_right_diagonal_from :d5

          fields.should eq [:c6, :b7, :a8]
        end

        its '#lower_right_diagonal_from' do
          fields = board.lower_right_diagonal_from :b3

          fields.should eq [:a2]
        end

        its '#up_from' do
          fields = board.up_from :c4

          fields.should eq [:c5, :c6, :c7, :c8]
        end

        its '#right_from' do
          fields = board.right_from :e3

          fields.should eq [:f3, :g3, :h3]
        end

        its '#down_from' do
          fields = board.down_from :c3

          fields.should eq [:c2, :c1]
        end

        its '#left_from' do
          fields = board.left_from :b4

          fields.should eq [:a4]
        end
      end

      describe '#apply_move' do
        it 'works for basic moves' do
          board = create_board %w(
            -- -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- --
          )

          move = Move.new :e1, :e2

          board.apply_move move

          board[:e2].piece.should be_a Piece::King
          board[:e1].piece.should be_nil

          meta_move = board.scoresheet.last_white_move

          meta_move.piece.should be_a Piece::King
          meta_move.piece.times_moved.should be 1
        end

        it 'works with capturing moves' do
          board = create_board %w(
            -- -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- bN -- -- -- -- --
            -- wP -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- --
          )

          move = Move.new :b3, :c4

          board.apply_move move

          board[:c4].piece.should be_a Piece::Pawn
          board[:b3].piece.should be_nil

          meta_move = board.scoresheet.last_white_move

          meta_move.piece.should be_a Piece::Pawn
          meta_move.piece.times_moved.should be 1
          meta_move.captured.should be_a Piece::Knight
        end

        it 'works with capturing moves' do
          board = create_board %w(
            -- -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- bN -- -- -- -- --
            -- wP -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- --
          )

          move = Move.new :b3, :c4

          board.apply_move move

          board[:c4].piece.should be_a Piece::Pawn
          board[:b3].piece.should be_nil

          meta_move = board.scoresheet.last_white_move

          meta_move.piece.should be_a Piece::Pawn
          meta_move.piece.times_moved.should be 1
          meta_move.captured.should be_a Piece::Knight
        end

        it 'works when castling' do
          board = create_board %w(
            -- -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- wR
          )

          move = Move.new :e1, :g1

          board.apply_move move

          board[:g1].piece.should be_a Piece::King
          board[:f1].piece.should be_a Piece::Rook
          board[:h1].should be_empty
          board[:e1].should be_empty

          meta_move = board.scoresheet.last_white_move

          meta_move.piece.should be_a Piece::King
          meta_move.piece.times_moved.should be 1
        end

        it 'works with en passant' do
          board = create_board %w(
            -- -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- wP bP
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- --
          )

          board.scoresheet.moves = [ { black: Move.new(:h7, :h5) } ]

          move = Move.new :g5, :h6

          board.apply_move move

          board[:h6].piece.should be_a Piece::Pawn
          board[:h5].should be_empty
          board[:g5].should be_empty

          meta_move = board.scoresheet.last_white_move

          meta_move.piece.should be_a Piece::Pawn
          meta_move.piece.times_moved.should be 1
          meta_move.captured.should be_a Piece::Pawn
        end

        it 'handles promotions' do
          board = create_board %w(
            -- -- -- -- bK -- -- --
            -- -- -- -- -- -- wP --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- --
          )

          move = Move.new :g7, :g8, Piece::Queen.new(:white)

          board.apply_move move

          board[:g7].should be_empty
          board[:g8].piece.should be_a Piece::Queen
        end
      end

      describe '#updo_last_move' do
        it 'can undo basic moves' do
          board = create_board %w(
            -- -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- wP --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- --
          )

          board.apply_move Move.new(:g5, :g6)
          board.undo_last_move

          result = create_board %w(
            -- -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- wP --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- --
          )

          board.to_a.should == result.to_a
          board.scoresheet.moves.should be_empty
          board[:g5].piece.times_moved.should be 0
        end

        it 'can undo capturing' do
          board = create_board %w(
            -- -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            bP -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- wB -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- --
          )

          board.apply_move Move.new(:c4, :a6)
          board.undo_last_move

          result = create_board %w(
            -- -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            bP -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- wB -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- --
          )

          board.to_a.should == result.to_a
        end

        it 'can undo castling' do
          board = create_board %w(
            -- -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            wR -- -- -- wK -- -- --
          )

          move = Move.new :e1, :c1

          board.apply_move move
          board.undo_last_move

          result = create_board %w(
            -- -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            wR -- -- -- wK -- -- --
          )

          board.to_a.should == result.to_a
        end

        it 'can undo en passant' do
          board = create_board %w(
            -- -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- bP wP -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- --
          )

          board.scoresheet.moves = [ { black: Move.new(:b7, :b5) } ]

          move = Move.new :c5, :b6

          board.apply_move move
          board.undo_last_move

          result = create_board %w(
            -- -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- bP wP -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- --
          )

          board.to_a.should == result.to_a
        end

        it 'can undo promotions' do
          board = create_board %w(
            -- -- -- -- bK -- -- --
            -- wP -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- --
          )

          move = Move.new :b7, :b8, Piece::Queen.new(:white)

          board.apply_move move
          board.undo_last_move

          result = create_board %w(
            -- -- -- -- bK -- -- --
            -- wP -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- --
          )

          board.to_a.should == result.to_a
        end
      end
    end
  end
end
