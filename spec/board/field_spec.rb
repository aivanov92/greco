require 'spec_helper'

module Greco
  module Chess
    describe Board::Field do
      let(:board) { Board.new }

      it '#empty?' do
        board[:e4].should be_empty

        board[:e4].piece = Piece::Rook.new :white
        board[:e4].should_not be_empty
      end

      it '#piece= calls the piece\'s #field=' do
        king = Piece::King.new :white
        field = board[:e1]

        king.should_receive(:field=).with(field)

        field.piece = king
      end

      it '#set_piece' do
        king = Piece::King.new :white
        field = board[:e1]

        field.set_piece king
        king.field.should be nil
      end

      describe '#==' do
        let(:other_field) { board[:d6] }

        subject(:e1_field) { board[:e1] }

        it { should eq e1_field }
        it { should eq :e1 }
        it { should_not eq other_field }
      end
    end
  end
end
