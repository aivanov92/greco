require 'spec_helper'

module Greco
  module Chess
    describe Validator do
      include TestUtils
      describe '#check?' do
        context 'when moving the king' do
          context 'with white' do
            let (:board) do
              create_board %w(
                -- bK -- -- -- -- -- --
                -- -- -- -- -- -- -- --
                -- -- -- -- -- -- -- --
                -- -- -- -- -- -- -- --
                -- -- -- -- -- -- -- --
                -- -- -- -- -- -- -- --
                -- -- -- bR wP -- -- --
                -- -- -- -- wK -- -- wR
              )
            end

            it 'is ok to move on non-attacked field' do
              is_ok_to_move? board, Move.new(:e1, :f1)
            end

            it 'is ok to capture enemy figures' do
              is_ok_to_move? board, Move.new(:e1, :d2)
            end

            it 'is not ok to capture own figures' do
              is_not_ok_to_move? board, Move.new(:e1, :e2)
            end
          end
        end
      end
    end
  end
end
