require 'spec_helper'

module Greco
  module Chess
    describe Validator do
      include TestUtils

      context 'when moving a pawn' do
        describe '#check?' do
          context 'with white' do
            let (:board) do
              create_board %w(
                -- bK -- -- -- -- -- --
                -- -- -- -- wP -- -- bP
                -- -- -- -- -- -- -- --
                bP wP bP bP -- wR bP --
                -- -- -- -- -- wP -- --
                -- -- -- -- -- -- -- bP
                -- -- -- wP -- -- -- wP
                -- wK -- -- -- -- -- --
              )
            end

            it 'is ok to make move by 1' do
              is_ok_to_move? board, Move.new(:d2, :d3)
            end

            it 'is not ok to move to left/right without capturing' do
              is_not_ok_to_move? board, Move.new(:d2, :c3)
            end

            it 'is ok to jump over 2 squares if the pawn is on the first line' do
              is_ok_to_move? board, Move.new(:d2, :d4)
            end

            describe 'cannot move over pieces' do
              it { is_not_ok_to_move? board, Move.new(:c4, :c5) }

              it { is_not_ok_to_move? board, Move.new(:h2, :h4) }
            end

            it 'is ok to capture pieces' do
              is_ok_to_move? board, Move.new(:f4, :g5)
            end

            it 'is ok to promote pieces' do
              is_ok_to_move? board, Move.new(:e7, :e8, Piece::Rook.new(:white))
            end

            it 'is not to promote pieces of different color' do
              is_not_ok_to_move? board, Move.new(:e7, :e8, Piece::Rook.new(:black))
            end

            it 'is not ok to promote in the middle of the board' do
              is_not_ok_to_move? board, Move.new(:b5, :b6, Piece::Queen.new(:white))
            end

            it 'is ok to make en passant' do
              board.scoresheet.moves = [ { black: Move.new(:a7, :a5) } ]
              is_ok_to_move? board, Move.new(:b5, :a6)
            end

            it 'is not ok to make en passant looking move' do
              board.scoresheet.moves = [ { black: Move.new(:a7, :a5) } ]
              is_not_ok_to_move? board, Move.new(:b5, :c6)
            end

            it 'is not ok to move a pawn backward' do
              is_not_ok_to_move? board, Move.new(:b5, :b4)
            end
          end

          context 'with black' do
            let (:board) do
              board = create_board %w(
                -- bK -- -- -- -- -- --
                -- -- -- -- -- -- -- bP
                -- -- -- -- -- -- wN --
                -- -- -- -- -- -- -- --
                -- -- -- wP bP wP -- --
                -- -- -- -- -- -- -- --
                -- -- -- -- -- -- -- bP
                -- wK -- -- -- -- -- --
              )

              board.scoresheet.moves = [ { white: Move.new(:a1, :b1) } ]
              board
            end

            it 'is ok to make en passant with black' do
              board.scoresheet.moves = [ { white: Move.new(:f2, :f4) } ]
              is_ok_to_move? board, Move.new(:e4, :f3)
            end

            it 'is bad en passant' do
              board.scoresheet.moves = [ { white: Move.new(:f2, :f4) } ]
              is_not_ok_to_move? board, Move.new(:e4, :d3)
            end

            it 'is not ok to move a pawn backward' do
              is_not_ok_to_move? board, Move.new(:e4, :e5)
            end

            it 'is ok to move a pawn forward' do
              is_ok_to_move? board, Move.new(:e4, :e3)
            end

            it 'is ok to move a pawn forward 2 fields from the start' do
              is_ok_to_move? board, Move.new(:h7, :h5)
            end

            it 'is ok to do capturing' do
              is_ok_to_move? board, Move.new(:h7, :g6)
            end

            it 'is ok to do promoting' do
              is_ok_to_move? board, Move.new(:h2, :h1, Piece::Knight.new(:black))
            end

            it 'is not ok to promote a figure in another color' do
              is_not_ok_to_move? board, Move.new(:h2, :h1, Piece::Knight.new(:white))
            end
          end
        end
      end
    end
  end
end
