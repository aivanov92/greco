require 'spec_helper'

module Greco
  module Chess
    describe Validator do
      include TestUtils

      describe '#check?' do
        let (:board) do
          create_board %w(
            -- bK -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- bP -- wR -- --
            -- -- -- -- -- wP -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- wK -- -- -- -- -- --
          )
        end

        it 'is not ok to make a move without figure' do
          is_not_ok_to_move? board, Move.new(:h8, :h7)
        end

        it 'is not ok to play black when white are on move' do
          is_not_ok_to_move? board, Move.new(:b8, :a7)
        end

        it 'is ok to make normal moves' do
          is_ok_to_move? board, Move.new(:f5, :f7)
        end

        it 'is ok to make capturing moves' do
          is_ok_to_move? board, Move.new(:f5, :d5)
        end

        it 'is not ok to move over figures' do
          is_not_ok_to_move? board, Move.new(:f5, :c5)
        end

        it 'is not ok to capture my own figures' do
          is_not_ok_to_move? board, Move.new(:f5, :f4)
        end
      end
    end
  end
end
