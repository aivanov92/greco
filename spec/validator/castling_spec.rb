require 'spec_helper'

module Greco
  module Chess
    describe Validator do
      include TestUtils

      it 'is not possible to castle without a rook' do
        board = create_board %w(
          -- bK -- -- -- -- -- --
          -- -- -- -- -- -- -- --
          -- -- -- -- -- -- -- --
          -- -- -- -- -- -- -- --
          -- -- -- -- -- -- -- --
          -- -- -- -- -- -- -- --
          -- -- -- -- -- -- -- --
          -- -- -- -- wK -- -- --
        )

        is_not_ok_to_move? board, Move.new(:e1, :g1)
      end

      describe 'short castle with white' do
        let (:board) do
          create_board %w(
            -- bK -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- wR
          )
        end

        let(:castle) { Move.new :e1, :g1 }

        it { is_ok_to_move? board, castle }

        context 'when having a piece in the way' do
          it 'on f1' do
            board[:f1].piece = Piece::Knight.new :white
            is_not_ok_to_move? board, castle
          end

          it 'on g1' do
            board[:g1].piece = Piece::Knight.new :white
            is_not_ok_to_move? board, castle
          end
        end

        context 'when the king is moved' do
          it 'is not a valid move' do
            board[:e1].piece.times_moved = 2

            is_not_ok_to_move? board, castle
          end
        end

        context 'when the rook is moved' do
          it 'is not a valid move' do
            board[:h1].piece.times_moved = 2

            is_not_ok_to_move? board, castle
          end
        end

        it 'is not ok if the king is under attack' do
          board = create_board %w(
            -- bK -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- bN -- -- -- -- --
            -- -- -- -- wK -- -- wR
          )

          is_not_ok_to_move? board, castle
        end

        it 'is not ok if the field f1 is attacked' do
          board = create_board %w(
            -- bK -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- bN -- -- -- --
            -- -- -- -- wK -- -- wR
          )

          is_not_ok_to_move? board, castle
        end
      end

      describe 'short castle with black' do
        let (:board) do
          board = create_board %w(
            -- -- -- -- bK -- -- bR
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- --
          )

          board.scoresheet.moves = [ { white: Move.new(:d1, :e1) } ]
          board
        end

        let(:castle) { Move.new :e8, :g8 }

        it { is_ok_to_move? board, castle }

        context 'when having a piece in the way' do
          it 'on f8' do
            board[:f8].piece = Piece::Knight.new :white
            is_not_ok_to_move? board, castle
          end

          it 'on g8' do
            board[:g8].piece = Piece::Knight.new :white
            is_not_ok_to_move? board, castle
          end
        end

        context 'when the king is moved' do
          it 'is not a valid move' do
            board[:e8].piece.times_moved = 2

            is_not_ok_to_move? board, castle
          end
        end

        context 'when the rook is moved' do
          it 'is not a valid move' do
            board[:h8].piece.times_moved = 2

            is_not_ok_to_move? board, castle
          end
        end

        it 'is not ok to castle if the king is under attack' do
          board = create_board %w(
            -- -- -- -- bK -- -- bR
            -- -- wN -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- --
          )
          board.scoresheet.moves = [ { white: Move.new(:d5, :c7) } ]

          is_not_ok_to_move? board, castle
        end

        it 'is not ok if the field f1 is attacked' do
          board = create_board %w(
            -- -- -- -- bK -- -- bR
            -- -- -- wN -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- --
          )
          board.scoresheet.moves = [ { white: Move.new(:e5, :d7) } ]

          is_not_ok_to_move? board, castle
        end
      end

      describe 'long castle with white' do
        let (:board) do
          create_board %w(
            -- bK -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            wR -- -- -- wK -- -- --
          )
        end

        let(:castle) { Move.new :e1, :c1 }

        it { is_ok_to_move? board, castle }

        context 'when having a piece in the way' do
          it 'on d1' do
            board[:d1].piece = Piece::Knight.new :white
            is_not_ok_to_move? board, castle
          end

          it 'on c1' do
            board[:c1].piece = Piece::Knight.new :white
            is_not_ok_to_move? board, castle
          end

          it 'on b1' do
            board[:b1].piece = Piece::Knight.new :white
            is_not_ok_to_move? board, castle
          end
        end

        context 'when the king is moved' do
          it 'is not a valid move' do
            board[:e1].piece.times_moved = 2

            is_not_ok_to_move? board, castle
          end
        end

        context 'when the rook is moved' do
          it 'is not a valid move' do
            board[:a1].piece.times_moved = 2

            is_not_ok_to_move? board, castle
          end
        end

        it 'is not ok if the king is under attack' do
          board = create_board %w(
            -- bK -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- bN -- --
            -- -- -- -- -- -- -- --
            wR -- -- -- wK -- -- --
          )

          is_not_ok_to_move? board, castle
        end

        it 'is not ok if the field d1 is attacked' do
          board = create_board %w(
            -- bK -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- bN -- -- --
            -- -- -- -- -- -- -- --
            wR -- -- -- wK -- -- --
          )

          is_not_ok_to_move? board, castle
        end
      end

      describe 'long castle with black' do
        let (:board) do
          board = create_board %w(
            bR -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- --
          )

          board.scoresheet.moves = [ { white: Move.new(:d1, :e1) } ]

          board
        end

        let(:castle) { Move.new :e8, :c8 }

        it { is_ok_to_move? board, castle }

        context 'when having a piece in the way' do
          it 'on d8' do
            board[:d8].piece = Piece::Knight.new :white
            is_not_ok_to_move? board, castle
          end

          it 'on c8' do
            board[:c8].piece = Piece::Knight.new :white
            is_not_ok_to_move? board, castle
          end

          it 'on b8' do
            board[:b8].piece = Piece::Knight.new :white
            is_not_ok_to_move? board, castle
          end
        end

        context 'when the king is moved' do
          it 'is not a valid move' do
            board[:e8].piece.times_moved = 2

            is_not_ok_to_move? board, castle
          end
        end

        context 'when the rook is moved' do
          it 'is not a valid move' do
            board[:a8].piece.times_moved = 2

            is_not_ok_to_move? board, castle
          end
        end

        it 'is not ok if the king is under attack' do
          board = create_board %w(
            bR -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- wN -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- --
          )
          board.scoresheet.moves = [ { white: Move.new(:h5, :f6) } ]

          is_not_ok_to_move? board, castle
        end

        it 'is not ok if the field d1 is attacked' do
          board = create_board %w(
            bR -- -- -- bK -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wN -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- -- -- -- --
            -- -- -- -- wK -- -- --
          )
          board.scoresheet.moves = [ { white: Move.new(:d1, :e1) } ]

          is_not_ok_to_move? board, castle
        end
      end
    end
  end
end
