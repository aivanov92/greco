require 'spec_helper'

module Greco
  module Chess
    describe Coordinates do
      it '#new' do
        e4_from_120 = Coordinates.new 65
        e4_from_an = Coordinates.new :e4
        e4_from_other = Coordinates.new e4_from_120

        e4_from_120.should eq e4_from_an
        e4_from_120.should eq e4_from_other
      end

      describe 'convertions' do
        let(:coordinate) { Coordinates.new(32) }

        it '#to_an' do
          coordinate.to_an.should eq :b7
        end

        it '#to_120' do
          coordinate.to_120.should eq 32
        end
      end

      describe '#==' do
        let(:f6) { Coordinates.new :f6 }
        let(:d5) { Coordinates.new :d5 }

        it { expect(f6 == Coordinates.new(:f6)).to be true }
        it { expect(f6 == d5).to be false }

        it 'could compare with symbol' do
          expect(f6 == :f6).to be true
        end

        it 'could compare with numbers' do
          expect(f6 == 46).to be true
        end
      end
    end
  end
end
